<?xml version="1.0" encoding="UTF-8"?>
<project xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns="http://maven.apache.org/POM/4.0.0"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <parent>
        <groupId>org.noear</groupId>
        <artifactId>solon-parent</artifactId>
        <version>2.6.5</version>
    </parent>

    <groupId>com.gitee.peigenlpy</groupId>
    <artifactId>mica-mqtt-solon-plugin-parent</artifactId>
    <version>2.2.5</version>

    <name>mica-mqtt-solon-plugin</name>
    <description>
        mica-mqtt-solon-plugin is a Maven project based on Solon, plugin for Mica-Mqtt
    </description>
    <url>https://gitee.com/peigenlpy/mica-mqtt-solon-plugin</url>

    <licenses>
        <license>
            <name>木兰宽松许可证, 第2版</name>
            <url>http://license.coscl.org.cn/MulanPSL2</url>
            <distribution>mica-mqtt-solon-plugin</distribution>
        </license>
    </licenses>

    <developers>
        <developer>
            <name>peigen</name>
            <email>peigen123@gmail.com</email>
            <organization>https://peigen.info</organization>
            <url>https://gitee.com/peigenlpy</url>
        </developer>
    </developers>

    <scm>
        <connection>scm:git:git@gitee.com:peigenlpy/mica-mqtt-solon-plugin.git</connection>
        <developerConnection>scm:git:git@gitee.com:peigenlpy/mica-mqtt-solon-plugin.git</developerConnection>
        <url>https://gitee.com/peigenlpy/mica-mqtt-solon-plugin</url>
        <tag>master</tag>
    </scm>

    <modules>
        <module>mica-mqtt-client-solon-plugin</module>
        <module>mica-mqtt-server-solon-plugin</module>
    </modules>

    <packaging>pom</packaging>

    <properties>
        <revision>2.2.2</revision>
        <java.version>11</java.version>
        <compile.version>11</compile.version>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
        <maven.compiler.source>11</maven.compiler.source>
        <maven.compiler.target>11</maven.compiler.target>
        <mica-net.version>0.0.9</mica-net.version>
    </properties>

    <dependencyManagement>
        <dependencies>
            <dependency>
                <groupId>com.gitee.peigenlpy</groupId>
                <artifactId>mica-mqtt-client-solon-plugin</artifactId>
                <version>${revision}</version>
            </dependency>
            <dependency>
                <groupId>com.gitee.peigenlpy</groupId>
                <artifactId>mica-mqtt-server-solon-plugin</artifactId>
                <version>${revision}</version>
            </dependency>

            <dependency>
                <groupId>net.dreamlu</groupId>
                <artifactId>mica-mqtt-client</artifactId>
                <version>${revision}</version>
            </dependency>

            <dependency>
                <groupId>net.dreamlu</groupId>
                <artifactId>mica-mqtt-server</artifactId>
                <version>${revision}</version>
            </dependency>

            <dependency>
                <groupId>net.dreamlu</groupId>
                <artifactId>mica-net-http</artifactId>
                <version>${mica-net.version}</version>
            </dependency>

            <dependency>
                <groupId>cn.hutool</groupId>
                <artifactId>hutool-all</artifactId>
                <version>${hutool.version}</version>
            </dependency>

            <dependency>
                <groupId>com.alibaba.fastjson2</groupId>
                <artifactId>fastjson2</artifactId>
                <version>${fastjson2.version}</version>
                <scope>compile</scope>
            </dependency>
        </dependencies>
    </dependencyManagement>

    <build>
        <plugins>
            <!-- 指定JDK编译版本 -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <version>3.8.1</version>
                <configuration>
                    <source>${compile.version}</source>
                    <target>${compile.version}</target>
                    <encoding>UTF-8</encoding>
                </configuration>
            </plugin>
            <!-- 打包跳过测试 -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-surefire-plugin</artifactId>
                <version>3.0.0-M5</version>
                <configuration>
                    <skipTests>true</skipTests>
                </configuration>
            </plugin>
            <!-- 输出源代码包 -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-source-plugin</artifactId>
                <version>3.2.1</version>
                <executions>
                    <execution>
                        <id>attach-sources</id>
                        <goals>
                            <goal>jar</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
            <!-- 避免font文件的二进制文件格式压缩破坏 -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-resources-plugin</artifactId>
                <version>3.2.0</version>
                <configuration>
                    <nonFilteredFileExtensions>
                        <nonFilteredFileExtension>woff</nonFilteredFileExtension>
                        <nonFilteredFileExtension>woff2</nonFilteredFileExtension>
                        <nonFilteredFileExtension>eot</nonFilteredFileExtension>
                        <nonFilteredFileExtension>ttf</nonFilteredFileExtension>
                        <nonFilteredFileExtension>svg</nonFilteredFileExtension>
                    </nonFilteredFileExtensions>
                </configuration>
            </plugin>
        </plugins>
        <resources>
            <resource>
                <directory>src/main/resources</directory>
                <excludes>
                    <exclude>/doc/**/*.*</exclude>
                    <exclude>/sql/*.*</exclude>
                    <exclude>/shell/*.*</exclude>
                </excludes>
                <filtering>false</filtering>
            </resource>
            <resource>
                <directory>src/main/java</directory>
                <includes>
                    <include>**/*.xml</include>
                    <include>**/*.json</include>
                    <include>**/*.ftl</include>
                </includes>
            </resource>
        </resources>
    </build>

    <profiles>
        <profile>
            <id>rdc</id>
            <properties>
                <altReleaseDeploymentRepository>
                    rdc-releases::default::https://packages.aliyun.com/maven/repository/2283586-release-vSuD8n/
                </altReleaseDeploymentRepository>
                <altSnapshotDeploymentRepository>
                    rdc-snapshots::default::https://packages.aliyun.com/maven/repository/2283586-snapshot-h1llab/
                </altSnapshotDeploymentRepository>
            </properties>
        </profile>
    </profiles>

    <repositories>
        <repository>
            <id>rdc-releases</id>
            <url>https://packages.aliyun.com/maven/repository/2283586-release-vSuD8n/</url>
            <releases>
                <enabled>true</enabled>
            </releases>
            <snapshots>
                <enabled>false</enabled>
            </snapshots>
        </repository>
        <repository>
            <id>rdc-snapshots</id>
            <url>https://packages.aliyun.com/maven/repository/2283586-snapshot-h1llab/</url>
            <releases>
                <enabled>false</enabled>
            </releases>
            <snapshots>
                <enabled>true</enabled>
            </snapshots>
        </repository>
        <repository>
            <id>tencent</id>
            <url>https://mirrors.cloud.tencent.com/nexus/repository/maven-public/</url>
            <snapshots>
                <enabled>false</enabled>
            </snapshots>
        </repository>
    </repositories>

</project>
